import sys
import logging
from epics import PV

from eco_components.namespace import NamespaceCollection
from eco_components.ecoinit import ecoinit

from devices import components, config


_scope_name = "bernina"
_namespace = globals()
_mod = sys.modules[__name__]
alias_namespaces = NamespaceCollection()


ecoinit(_mod=_mod, alias_namespaces=alias_namespaces, components=components)



