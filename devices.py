# # New configuration of components:
# components is an ordered list of
# - name in parent package
# - type, describing the python Class or factory function.
# - arguments of that type args
# - kwargs of that type

# # Conventions for the type
# the call of type will try to pass a kwarg 'name' with the
# name of the component, before only calling args and kwargs.
# if arg or kwarg is of type config.Component (dummy class)
# this indicates that an earlier initialized object is used
# (e.g. from same configuration).
from eco_components.config import (
    Component,
    Alias,
    init_device,
    initFromConfigList,
    Configuration,
)

_eco_lazy_init = False

config = Configuration(
    "./bernina/config/config.json", name="bernina_config"
)

components = [
    #        {
    #            'name'  : 'device_alias_name',
    #            'type'  : 'package.module.submodule:ClassOrFactory',
    #            'args'  : ['all','the','requires','args'],
    #            'kwargs': {}
    #            }
    {
        "type": "eco_components.config:append_to_path",
        "args": config["path_exp"],
        "name": "path_exp",
        "kwargs": {},
        "lazy": False,
    },
    {
        "name": "elog",
        "type": "slic.utils.elog:Elog",
        "args": ["https://elog-gfa.psi.ch/Bernina"],
        "kwargs": {
            "user": "gac-bernina",
            "screenshot_directory": "/sf/bernina/config/screenshots",
            "password": "supercorrect"
        },
    },
    {
        "name": "screenshot",
        "type": "slic.utils.elog:Screenshot",
        "args": [],
        "kwargs": {"screenshot_directory": "/sf/bernina/config/screenshots"},
    },
    {
        "name": "slit_und",
        "type": "slic.devices.xoptics.slits:SlitTwinUnit",
        "args": ["SARFE10-OAPU044"],
        "kwargs": {},
        "desc": "Slit after Undulator",
    },
    {
        "name": "att_fe",
        "type": "slic.devices.xoptics.aramis_attenuator:Attenuator",
        "args": ["SARFE10-OATT053"],
        "kwargs": {},
        "z_und": 53,
        "desc": "Attenuator in Front End",
    },
    {
        "name": "mon_und",
        "type": "slic.devices.xdiagnostics.intensitymonitor:IntensityMonitorPBPS",
        "args": ["SARFE10-PBPS053"],
        "kwargs": {},
        "z_und": 53,
        "desc": "Intensity/position monitor after Optics hutch",
    },
    {
        "name": "prof_fe",
        "args": ["SARFE10-PPRM064"],
        "kwargs": {},
        "z_und": 64,
        "desc": "Profile monitor after Front End",
        "type": "slic.devices.xdiagnostics.profilemonitor:PPRM",
    },
    {
        "name": "prof_mirr_alv1",
        "args": ["SAROP11-PPRM066"],
        "kwargs": {},
        "z_und": 66,
        "desc": "Profile monitor after Alvra Mirror 1",
        "type": "slic.devices.xdiagnostics.profilemonitor:PPRM",
    },
    {
        "name": "slit_switch",
        "z_und": 92,
        "desc": "Slit in Optics hutch after Photon switchyard and before Bernina optics",
        "type": "slic.devices.xoptics.slits:SlitUnit",
        "args": ["SAROP21-OAPU092"],
        "kwargs": {},
    },
    {
        "name": "prof_mirr1",
        "args": ["SAROP21-PPRM094"],
        "kwargs": {},
        "z_und": 94,
        "desc": "Profile monitor after Mirror 1",
        "type": "slic.devices.xdiagnostics.profilemonitor:PPRM",
    },
    {
        "name": "mirr1",
        "args": [],
        "kwargs": {},
        "z_und": 92,
        "desc": "Vertical offset mirror 1",
        "type": "slic.devices.xoptics.offsetmirrors:OffsetMirror",
        "kwargs": {"ID": "SAROP21-OOMV092"},
    },
    {
        "name": "mirr2",
        "args": [],
        "kwargs": {},
        "z_und": 96,
        "desc": "Vertical offset mirror 2",
        "type": "slic.devices.xoptics.offsetmirrors:OffsetMirror",
        "kwargs": {"ID": "SAROP21-OOMV096"},
    },
    {
        "name": "mono",
        "args": ["SAROP21-ODCM098"],
        "kwargs": {},
        "z_und": 98,
        "desc": "DCM Monochromator",
        "type": "slic.devices.xoptics.dcm:DoubleCrystalMono",
    },
    {
        "name": "prof_mono",
        "args": ["SAROP21-PPRM102"],
        "kwargs": {},
        "z_und": 102,
        "desc": "Profile monitor after Monochromator",
        "type": "slic.devices.xdiagnostics.profilemonitor:PPRM",
    },
    {
        "name": "xp",
        "args": [
            "SAROP21-OPPI103",
            "SGE-CPCW-72-EVR0:FrontUnivOut15"
        ],
        "kwargs": {
            "name": "X-ray pulse picker",
        },
        "z_und": 103,
        "type": "slic.devices.xoptics.pulsepicker:PulsePicker",
        "lazy": False,
    },
    {
        "name": "mon_opt_old",
        "type": "slic.devices.xdiagnostics.intensitymonitor:IntensityMonitorPBPS",
        "args": ["SAROP21-PBPS133"],
        "kwargs": {"VME_crate": "SAROP21-CVME-PBPS1"},
        "z_und": 133,
        "desc": "Intensity/position monitor after Optics hutch",
    },
    {
        "name": "mon_opt",
        "type": "slic.devices.xdiagnostics.intensitymonitor:IntensityMonitorPBPS",
        "args": ["SAROP21-PBPS133"],
        "kwargs": {
            'VME_crate': 'SAROP21-CVME-PBPS1',
            'calib_channels': {
                'up': 'SLAAR21-LSCP1-FNS:CH6:VAL_GET',
                'down': 'SLAAR21-LSCP1-FNS:CH7:VAL_GET',
                'left': 'SLAAR21-LSCP1-FNS:CH4:VAL_GET',
                'right': 'SLAAR21-LSCP1-FNS:CH5:VAL_GET'
            },
            'calc_channels': {
                'itot': 'SLAAR21-LTIM01-EVR0:CALCI',
                'xpos': 'SLAAR21-LTIM01-EVR0:CALCX',
                'ypos': 'SLAAR21-LTIM01-EVR0:CALCY'
            }
        },
        "z_und": 133,
        "desc": "Intensity/position monitor after Optics hutch",
    },
    {
        "name": "prof_opt",
        "args": ["SAROP21-PPRM133"],
        "kwargs": {},
        "z_und": 133,
        "desc": "Profile monitor after Optics hutch",
        "type": "slic.devices.xdiagnostics.profilemonitor:PPRM",
    },
    {
        "name": "spect_tt",
        "type": "slic.devices.xdiagnostics.timetools:SpectralEncoder",
        "args": ["SAROP21-PSEN135"],
        "kwargs": {"reduction_client_address": "http://sf-daqsync-02:12002/"},
        "z_und": 135,
        "desc": "Spectral encoding timing diagnostics before Attenuator.",
        "lazy":False,
    },
    {
        "name": "att",
        "type": "slic.devices.xoptics.aramis_attenuator:Attenuator",
        "args": ["SAROP21-OATT135"],
        "kwargs": {},
        "z_und": 135,
        "desc": "Attenuator Bernina",
    },
    {
        "name": "ref_laser",
        "args": ["SAROP21-OLAS136"],
        "kwargs": {},
        "z_und": 136,
        "desc": "Bernina beamline reference laser before KBs",
        "type": "slic.devices.xoptics.aramis_reflaser:RefLaser",
    },
    {
        "name": "slit_att",
        "args": ["SAROP21-OAPU136"],
        "kwargs": {},
        "z_und": 136,
        "desc": "Slits behind attenuator",
        "type": "slic.devices.xoptics.slits:SlitUnitCenterWidth",
        "lazy": True,
    },
    {
        "name": "mon_att",
        "type": "slic.devices.xdiagnostics.intensitymonitor:IntensityMonitorPBPS", 
        "args": ["SAROP21-PBPS138"],
        "kwargs": {"VME_crate": "SAROP21-CVME-PBPS2"},
        "z_und": 138,
        "desc": "Intensity/Position monitor after Attenuator",
    },
    {
        "name": "det_dio",
        "type": "slic.devices.general.detectors:DiodeDigitizer",
        "args": ["SAROP21-PDIO138", "SAROP21-CVME-PBPS2"],
        "kwargs": {},
        "z_und": 138,
        "desc": "Diode digitizer for exp data",
    },
    {
        "name": "prof_att",
        "args": ["SAROP21-PPRM138"],
        "kwargs": {},
        "z_und": 138,
        "desc": "Profile monitor after Attenuator",
        "type": "slic.devices.xdiagnostics.profilemonitor:PPRM",
    },
    {
        "name": "kb_ver",
        "args": ["SAROP21-OKBV139"],
        "z_und": 139,
        "desc": "Vertically focusing Bernina KB mirror",
        "type": "slic.devices.xoptics.kb:KBVer",
        "kwargs": {},
    },
    {
        "args": ["SAROP21-OKBH140"],
        "name": "kb_hor",
        "z_und": 140,
        "desc": "Horizontally focusing Bernina KB mirror",
        "type": "slic.devices.xoptics.kb:KBHor",
        "kwargs": {},
    },
    {
        "name": "spatial_tt",
        "type": "slic.devices.xdiagnostics.timetools:SpatialEncoder",
        "args": [],
        "kwargs": {"reduction_client_address": "http://sf-daqsync-02:12003/"},
        "z_und": 141,
        "desc": "spatial encoding timing diagnostics before sample.",
        "lazy":False,
    },
     {
         "name": "slit_kb",
         "args": [],
         "kwargs": {"pvname": "SARES20-MF1"},
         "z_und": 141,
         "desc": "Slits behind Kb",
         "type": "slic.devices.xoptics.slits:SlitUnitJJ",
     },
    {
        "name": "gps",
        "type": "slic.devices.endstations.bernina_platform:BerninaPlatform",
        "args": ["SARES22-GPS"],
        "kwargs": {"configuration": config["gps_config"]},
        "desc": "General purpose station",
        "z_und": 142,
    },
    {
        "name": "xrd",
        "type": "slic.devices.endstations.bernina_platform:BerninaPlatform",
        "args": ["SARES21-XRD"],
        "kwargs": {"configuration": config["xrd_config"]},
        "desc": "Xray diffractometer",
        "z_und": 142,
    },
    
    {
        "args": [],
        "name": "vonHamos",
        "z_und": 142,
        "desc": "Kern experiment, von Hamos vertical and horizontal stages ",
        "type": "slic.devices.general.micos_stage:stage",
        "kwargs": {
            "vonHamos_horiz_pv": config["Kern"]["vonHamos_horiz"],
            "vonHamos_vert_pv": config["Kern"]["vonHamos_vert"],}
    },
    {
        "args": [],
        "name": "xeye",
        "z_und": 142,
        "desc": "Mobile X-ray eye in Bernina hutch",
        "type": "slic.devices.endstations.bernina_cameras:Xeye",
        "kwargs": {
            "zoomstage_pv": config["xeye"]["zoomstage_pv"],
            "camera_pv": config["xeye"]["camera_pv"],
            "bshost": "sf-daqsync-01.psi.ch",
            "bsport": 11151,
        },
        
    },
    {
        "args": [],
        "name": "cams_qioptiq",
        "z_und": 142,
        "desc": "Qioptic sample viewer in Bernina hutch",
        "type": "slic.devices.endstations.bernina_cameras:CameraQioptiq",
        "kwargs": {
            "bshost": "sf-daqsync-01.psi.ch",
            "bsport": 11149,
            "zoomstage_pv": config["cams_qioptiq"]["zoomstage_pv"],
            "camera_pv": config["cams_qioptiq"]["camera_pv"],
        },
    },
    {
        "args": [],
        "name": "cams_sigma",
        "z_und": 142,
        "desc": "Sigma objective",
        "type": "slic.devices.endstations.bernina_cameras:CameraSigma",
        "kwargs": {
            "bshost": "sf-daqsync-01.psi.ch",
            "bsport": 11149,
            "zoomstage_pvs": config["cams_sigma"]["zoomstage_pvs"],
            "focusstage_pvs": config["cams_sigma"]["focusstage_pvs"],
            "camera_pv": config["cams_sigma"]["camera_pv"],
        },
    },
    {
        "args": [
            "SLAAR02-TSPL-EPL",
            "/sf/bernina/config/eco/offsets"
        ],
        "name": "phase_shifter",
#        "z_und": 142,
        "name": "Experiment laser phase shifter",
        "type": "slic.devices.timing.lasertiming:PhaseShifterAramis",
        "kwargs": {},
    },
    {
        "args": [],
        "name": "las",
        "z_und": 142,
        "desc": "Experiment laser optics",
        "type": "slic.devices.loptics.bernina_explaser:ExpLaser",
        "kwargs": {"ID": "SLAAR21-LMOT", "smar_config": config["las_smar_config"]},
        "lazy": True,
    },
    {
        "name": "laser_shutter",
        "args": [
            "SLAAR21-LTIM01-EVR0:FrontUnivOut3_SOURCE"
        ],
        "kwargs": {
            "status_open": 4,
            "status_closed": 3,
            "name": "Laser Shutter",
        },
        "z_und": 142,
        "type": "slic.devices.loptics.lasershutter:LaserShutter",
    },
    {
        "args": [],
        "name": "epics_channel_list",
        "desc": "epics channel list",
        "type": "eco_components.config:ChannelList",
#        "kwargs": {"file_name":"/sf/bernina/config/channel_lists/default_channel_list_epics"},
        "kwargs": {"file_name":"./bernina/config/channel_list"},
    },
    {
        "args": [],
        "name": "epics_daq",
        "z_und": 142,
        "desc": "epics data acquisition",
        "type": "slic.core.acquisition:PVAcquisition",
        "kwargs": {
            "instrument": "bernina",
            "pgroup": config["pgroup"],
            "default_channels": Component("epics_channel_list"),
            "default_dir": f"/sf/bernina/data/{config['pgroup']}/res/epics_daq/",
        },
    },
    {
        "args": [],
        "name": "daq",
        "desc": "server based acquisition",
        "type": "slic.core.acquisition:DIAAcquisition",
        "kwargs": {
            "instrument": "bernina",
            "pgroup": config["pgroup"],
            "api_address": config["daq_address"],
#            "pedestal_directory": config["jf_pedestal_directory"],
#            "gain_path": config["jf_gain_path"],
#            "config_default": config["daq_dia_config"],
            "default_channels": config["jf_channels"],
            "default_dir": None,
        },
    },
    {
        "args": [
            config["checker_PV"],
            config["checker_thresholds"],
            config["checker_fractionInThreshold"],
        ],  #'SARFE10-PBPG050:HAMP-INTENSITY-CAL',[60,700],.7],
        "name": "checker",
        "desc": "checker functions for data acquisition",
        "type": "slic.core.condition:PVCondition",
        "kwargs": {
            "wait_time": 0.1,
            "required_fraction": 0.8,
        },
    },
    {
        "args": [],
        "name": "scans",
        "desc": "server based acquisition",
        "type": "slic.core.scanner:Scanner",
        "kwargs": {
            "data_base_dir": "scan_data",
            "scan_info_dir": f"/sf/bernina/data/{config['pgroup']}/res/scan_info",
            "default_acquisitions": [Component("daq")],
            "condition": Component("checker"),
            "make_scan_sub_dir": True,
        },
    },
    {
        "args": [],
        "name": "epics_scans",
        "desc": "epics non beam synchronous based acquisition",
        "type": "slic.core.scanner:Scanner",
        "kwargs": {
            "data_base_dir": "scan_data",
            "scan_info_dir": f"/sf/bernina/data/{config['pgroup']}/res/epics_daq/scan_info",
            "default_acquisitions": [Component("epics_daq")],
            "condition": Component("checker"),
            "make_scan_sub_dir": True,
        },
    },
    {
        "args": [
            "SLAAR02-TSPL-EPL",
            "SLAAR-LTIM02-EVR0",
            "/sf/bernina/config/eco/offsets"
        ],
        "name": "lxt",
        "desc": "laser timing with pockels cells and phase shifter",
        "type": "slic.devices.timing.lasertiming:LXT",
        "kwargs": {},
    },
    {
        "args": ["SAR-CCTA-ESB"],
        "name": "seq",
        "desc": "sequencer timing application (CTA)",
        "type": "slic.devices.timing.events:CTASequencer",
        "kwargs": {},
    },
    {
        "name": "event_master",
        "type": "slic.devices.timing.events:TimingMaster",
        "args": ["SIN-TIMAST-TMA"],
        "kwargs": {},
        "desc": "SwissFEL timing master information",
    },
    {
        "args": ["SARES20-CVME-01-EVR0"],
        "name": "evr_bernina",
        "desc": "Bernina event receiver",
        "type": "slic.devices.timing.events:EventReceiver",
        "kwargs": {},
        "lazy": True,
    },
    {
        "args": [],
        "name": "default_channel_list",
        "desc": "Bernina default channels, used in daq",
        "type": "eco_components.config:ChannelList",
#        "kwargs": {"file_name":"/sf/bernina/config/channel_lists/default_channel_list"},
        "kwargs": {"file_name":"./bernina/config/channel_list"},
        "lazy": False,
    },
    {
        "args": [],
        "name": "default_channel_list_bs",
        "desc": "Bernina default bs channels, used by bs_daq",
        "type": "eco_components.config:ChannelList",
#        "kwargs": {"file_name":"/sf/bernina/config/channel_lists/default_channel_list_bs"},
        "kwargs": {"file_name":"./bernina/config/channel_list"},
        "lazy": False,
    },
    {
        "args": [],
        "name": "channels_spectrometer_projection",
        "desc": "",
        "type": "eco_components.config:ChannelList",
#        "kwargs": {"file_name":"/sf/bernina/config/channel_lists/channel_list_PSSS_projection"},
        "kwargs": {"file_name":"./bernina/config/channel_list"},
        "lazy": False,
    },
    {
        "args": [],
        "name": "bs_daq",
        "desc": "bs daq writer (locally!)",
        "type": "slic.core.acquisition:BSAcquisition",
        "kwargs": {
            "instrument": "bernina",
            "pgroup": config["pgroup"],
            "default_channels": {
                "bernina_default_channels_bs": Component("default_channel_list_bs")
            },
            "default_dir": f"/sf/bernina/data/{config['pgroup']}/res/%s",
        },
        "lazy": False,
    },
    {
        "args": ["Slits", [Component('slit_und'),Component('slit_switch'),Component('slit_att'),Component('slit_kb')]],
        "name": "slits",
        "desc": "collection of all slits",
        "type": "slic.core.device:SimpleDevice",
        "kwargs": {},
        "lazy": False,
    },
]

try:
    components.extend(config["components"])
    print("Did append additional components!")
except:
    print("Could not append components from config.")


#### OLD STUFF TO BE TRANSFERRED OR DELETED ####
components_old = {
    "SARFE10-OPSH044": {
        "alias": "ShutUnd",
        "z_und": 44,
        "desc": "Photon shutter after Undulator",
    },
    "SARFE10-PBIG050": {
        "alias": "GasMon",
        "z_und": 50,
        "desc": "Gas Monitor Intensity",
    },
    "SARFE10-PBPS053": {
        "alias": "MonUnd",
        "z_und": 44,
        "desc": "Intensity position monitor after Undulator",
    },
    "SARFE10-SBST060": {
        "alias": "ShutFE",
        "z_und": 60,
        "desc": "Photon shutter in the end of Front End",
    },
    "SAROP11-OOMH064": {
        "alias": "MirrAlv1",
        "z_und": 64,
        "desc": "Horizontal mirror Alvra 1",
    },
    "SAROP21-PSCR097": {
        "alias": "ProfMirr2",
        "z_und": 97,
        "desc": "Profile Monitor after Mirror 2",
    },
    "SAROP21-OPPI103": {"alias": "Pick", "z_und": 103, "desc": "X-ray pulse picker"},
    "SAROP21-BST114": {
        "alias": "ShutOpt",
        "z_und": 114,
        "desc": "Shutter after Optics hutch",
    },
    "SAROP21-PALM134": {
        "alias": "TimTof",
        "z_und": 134,
        "desc": "Timing diagnostics THz streaking/TOF",
    },
    "SAROP21-PSEN135": {
        "alias": "TimRef",
        "z_und": 135,
        "desc": "Timing diagnostics spectral encoding of ref. index change",
    }
    #        'SLAAR21-LMOT' : {
    #                'alias' : 'Palm',
    #                'z_und' : 142,
    #                'desc' : 'Streaking arrival time monitor',
    #                'eco_type' : 'timing.palm.Palm'},
    #        'SLAAR21-LMOT' : {
    #                'alias' : 'Psen',
    #                'z_und' : 142,
    #                'desc' : 'Streaking arrival time monitor',
    #                'eco_type' : 'timing.psen.Psen'}
    #         = dict(
    #            alias = ''
    #            z_und =
    #            desc = ''},
}
